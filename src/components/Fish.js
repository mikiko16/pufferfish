import { Sprite, Spritesheet, Texture } from 'pixi.js';

export default class Fish extends Sprite {

    constructor() {
        super();
        this.name = 'fish';
        this.buttonMode = true;
        this.interactive = true;

        this.fish = Sprite.from('small');
        this.fish.x = 200;
        this.fish.y = 200;
        this.fish.interactive = true;
        this.fish.buttonMode = true;
        this.addChild(this.fish);

        this.fish.on('click', () => {
            this.expand(this.fish);
            setTimeout(() => { this.contract() }, 5000);
        })
    } 

    expand() {
        this.fish.scale.x = 1.5;
        this.fish.scale.y = 1.5;
        let texture = PIXI.Texture.from('big');
        this.fish.texture = texture;
        //this.texture = sprite1;
    }

    contract() {
        this.fish.scale.x = 1;
        this.fish.scale.y = 1;
        let texture = PIXI.Texture.from('small');
        this.fish.texture = texture;
    }
}